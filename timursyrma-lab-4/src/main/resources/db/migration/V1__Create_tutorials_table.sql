CREATE TABLE tutorials
(
    id          BIGSERIAL PRIMARY KEY,
    title       VARCHAR(255),
    description TEXT,
    published   BOOLEAN
);
